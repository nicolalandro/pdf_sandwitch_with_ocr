import io
import os
import shutil

from PIL import Image
import pytesseract
from tqdm import tqdm
from wand.image import Image as wi

FILENAME = "sample2.pdf"

print('read pdf...')
pdf = wi(filename=FILENAME, resolution=600)
print('convert pdf to images...')
pdfImage = pdf.convert('jpeg')

if not os.path.exists('my_folder'):
    os.makedirs('ocr_pages')
with tqdm(total=len(pdfImage.sequence), desc="Doing OCR to page") as pbar:
    for i, img in enumerate(pdfImage.sequence):
        imgPage = wi(image=img)
        imgBlob = imgPage.make_blob('jpeg')
        im = Image.open(io.BytesIO(imgBlob))
        PDF = pytesseract.image_to_pdf_or_hocr(im, lang='ita', config='', nice=0, extension='pdf')
        with open("./ocr_pages/page_%03d.pdf" % i, "w+b") as f:
            f.write(bytearray(PDF))
        pbar.update(1)

print('combine pdf...')
os.system("pdftk ./ocr_pages/*.pdf cat output newfile.pdf")
print('clean dir..')
shutil.rmtree('ocr_pages')
print('finish ok!')

