# Install

```
pip install -r requirements
sudo apt install tesseract-ocr tesseract-ocr-ita imagemagick
```
* edit `/etc/ImageMagick-6/policy.xml` at PDF line and the first lines modify with
```
  <policy domain="resource" name="temporary-path" value="/tmp"/>
  <policy domain="resource" name="map" value="4GiB"/>
  <policy domain="resource" name="memory" value="2GiB"/>
  <policy domain="resource" name="area" value="1GB"/>
  <policy domain="resource" name="disk" value="16EB"/>
  <policy domain="resource" name="file" value="768"/>
  <policy domain="resource" name="thread" value="4"/>
  <policy domain="resource" name="throttle" value="0"/>
  <policy domain="resource" name="time" value="3600"/>
  <policy domain="system" name="precision" value="6"/>
....  
  <policy domain="coder" rights="read" pattern="PDF" />
```
* download and install pdftk

edit main.py with your pdf
```
python main.py
```
